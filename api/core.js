"use strict";

const Vision = require("vision");
const Nunjucks = require("nunjucks");
const path = require("path");
const cheerio = require("cheerio");
const inert = require("inert");
const io = require("socket.io");
const request = require("request");
const requestPromise = require("request-promise");

class SetupServer {
  constructor(server) {
    this.server = server;
  }

  async staticAssets() {
    await this.server.register({
      plugin: inert
    });
    await this.server.route({
      path: "/public/{path*}",
      method: "GET",
      handler: {
        directory: {
          path: path.join(__dirname, "../client"),
          listing: false,
          index: false
        }
      }
    });
  }

  async views() {
    await this.server.register(Vision);
    this.server.views({
      engines: {
        html: {
          compile: (src, options) => {
            const template = Nunjucks.compile(src, options.environment);
            return context => {
              return template.render(context);
            };
          },
          prepare: (options, next) => {
            options.compileOptions.environment = Nunjucks.configure(
              options.path,
              { watch: false }
            );
            return next();
          }
        }
      },
      relativeTo: __dirname,
      path: path.join(__dirname, "../views")
    });
  }

  routes() {
    this.server.route({
      method: "GET",
      path: "/",
      handler: function(req, h) {
        return h.view("home");
      }
    });
    this.server.route({
      method: "GET",
      path: "/{all*}",
      handler: function(req, h) {
        return h.view("error");
      }
    });
  }

  socket() {
    const socket = io(this.server.listener);

    socket.on("connection", stream => {
      stream.on("d3 modules", libraries => {
        (async function scrap(libraries, i) {
          if (typeof libraries[i] !== "undefined") {
            // Cheerio setup
            const Scrap = await requestPromise({
              uri: `https://www.npmjs.com/package/${libraries[i]}`,
              transform: function(body) {
                return cheerio.load(body);
              }
            });

            // Send browser d3-package details
            socket.emit("npm data", {
              moduleName: libraries[i],
              weeklyDownload: Scrap(".package__counter___2s5Ci").text(),
              website: Scrap("p.truncate.black-80 a").attr("href"),
              installation: Scrap(".flex-auto.truncate.db span").text(),
              version: Scrap("p.fw6.mb3.mt2.truncate.black-80.f4")
                .text()
                .slice(0, 5),
              npmLink: `https://www.npmjs.com/package/${libraries[i]}`
            });

            if (i !== libraries.length) {
              i = i + 1;
              scrap(libraries, i);
            }
          }
        })(libraries, 0);
      });
    });
  }
}

module.exports = SetupServer;
