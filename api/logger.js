"use strict";

const winston = require("winston");

const loggerConfig = {
  transports: [
    new winston.transports.File({
      level: "debug",
      filename: "./appDebug.log",
      handleExceptions: true
    }),
    new winston.transports.Console({
      level: "debug",
      json: true,
      handleExceptions: true
    })
  ],
  exitOneError: false
};

const logger = new winston.Logger(loggerConfig);
module.exports = logger;
