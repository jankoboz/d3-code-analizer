"use strict";

const Hapi = require("hapi");
const SetupServer = require("./core");

const server = new Hapi.server({
  port: 5000,
  host: "localhost",
  routes: {
    files: {
      relativeTo: "../public"
    }
  }
});

const Setup = new SetupServer(server);

(async function init() {
  // Views
  await Setup.views();

  // Static assets
  await Setup.staticAssets();

  // Routes
  Setup.routes();

  // Socket
  Setup.socket();

  await server.start();
  console.log(`Server running at: ${server.info.uri}`);
})();

process.on("unhandledRejection", e => {
  console.log(e);
  process.exit(1);
});
