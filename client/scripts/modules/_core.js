"use strict";

/**
 *  @param  {Object[]}  d3 modules & methods
 */

class Core {
  constructor(d3methods) {
    this.library = d3methods;
    this.socket = io();
    this.code = "";
    this.prevCode = "_";
    this.analizedModules = {};
    this.analizedMethods = {};
    this.matchingMethods = [];
    this.uniqueModules = [];
    this.submit();
  }

  submit() {
    document.getElementById("d3-form").addEventListener("submit", e => {
      e.preventDefault();

      const d3code = document
        .getElementById("d3-textarea")
        .value.replace(/(\r\n\t|\n|\r\t| )/gm, "");
      const clickPurpose = document.getElementById("searchButton").innerHTML;

      // track click abuse
      if (d3code === this.prevCode && clickPurpose !== "change") {
        alert("This code already analized!");
        return this.animateSearchbar();
      }

      // no search, just reveal the search bar
      if (clickPurpose === "change") {
        return this.animateSearchbar();
      }

      if (clickPurpose === "update") {
        this.reset();
      }

      this.code = d3code;
      this.scan();
      this.prevCode = this.code;
    });
  }

  scan() {
    let modulesQty = Object.keys(this.library).length;

    while (modulesQty--) {
      let currentd3Module = Object.keys(this.library)[modulesQty];
      let methodsQty = this.library[currentd3Module].length;

      while (methodsQty--) {
        let currentMethod = this.library[currentd3Module][methodsQty];
        const methodToSearch = "." + currentMethod + "(";

        // if d3 method used in the code block
        if (this.code.includes(methodToSearch) === true) {
          this.matchingMethods.push(currentd3Module);
        }

        // quantity of same method being used
        const timesUsed = this.code.split(methodToSearch).length - 1;

        if (timesUsed > 0) {
          // usage of module frequency
          if (this.analizedModules[currentd3Module]) {
            this.analizedModules[currentd3Module] += timesUsed;
          } else {
            this.analizedModules[currentd3Module] = timesUsed;
          }

          // usage of method frequency
          this.analizedMethods[currentMethod] = timesUsed;
        }
      }
    }

    if (this.matchingMethods.length) {
      // hide the searchbar
      this.animateSearchbar();

      this.uniqueModules = this.matchingMethods.filter((item, pos) => {
        return this.matchingMethods.indexOf(item) == pos;
      });

      // Scrap npm for module's data
      this.scrap();

      // Create bar-chart for modules
      this.analizeModules();

      // Create pie-chart for methods
      this.analizeMethods();
    } else {
      alert("d3 library is not used in your code!");
    }
  }

  animateSearchbar() {
    document.getElementById("d3-textarea").classList.toggle("hide-textarea");
    document
      .getElementById("searchTitle")
      .classList.toggle("hide-search-title");
    document.getElementById("info").classList.toggle("slide-info");

    const container = document.getElementById("containerDiv");
    container.classList.toggle("reverse-gradient");
    container.classList.toggle("control-height");

    const btn = document.getElementById("searchButton");
    btn.innerHTML === "change"
      ? (btn.innerHTML = "update")
      : (btn.innerHTML = "change");
  }

  scrap() {
    this.socket.emit("d3 modules", this.uniqueModules);

    this.socket.on("npm data", data => {
      // append new elements to dom
      const moduleData = `
            <ul class="module">
               <li class="module__name">${data.moduleName}</li>
               <li class="module__install">${data.installation}</li>
               <li class="module__click">more information...</li>
               <li class="module__more">
                  <div class="module__more-download">weekly download: <span>${
                    data.weeklyDownload
                  }</span></div>
                  <div class="module__more-info"><a href=${
                    data.website
                  } target="_blank">${data.website}</a></div>
                  <div class="module__more-info"><a href=${
                    data.npmLink
                  } target="_blank">${data.npmLink}</a></div>
               </li>
            </ul>
         `;
      document.getElementById("package-container").innerHTML += moduleData;

      // attach click events
      const moreInfoButtons = document.querySelectorAll(".module__click");
      moreInfoButtons.forEach(elm => {
        if (typeof elm.onclick !== "function") {
          elm.addEventListener("click", () => {
            elm.innerHTML = "";
            Object.assign(elm.nextElementSibling.style, {
              height: "auto",
              overflow: "auto"
            });
          });
        }
      });
    });

    return false;
  }

  reset() {
    // reset svgs
    document.querySelectorAll(".svg").forEach(parent => {
      while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
      }
    });

    // empty npm data
    document.getElementById("package-container").innerHTML = "";

    // Reset properties
    this.matchingMethods = [];
    this.uniqueModules = [];
    this.analizedMethods = {};
    this.analizedModules = {};
  }

  analizeModules() {
    const self = this;
    const width = 650;
    const height = 200;
    const padding = 10;
    const barQuantity = Object.keys(self.analizedModules).length;
    const barWidth = width / barQuantity - 16;

    const svg = d3
      .select("#moduleSvg svg")
      .attr("width", width)
      .attr("height", height + 15);

    // Vertical scale
    const yScale = d3
      .scaleLinear()
      .domain([0, d3.max(Object.values(self.analizedModules))])
      .range([2, height - 35]);

    // Opacity scale
    const opacityScale = d3
      .scaleLinear()
      .domain([0, d3.max(Object.values(self.analizedModules))])
      .range([0.5, 1]);

    // Create bars
    (function createBars() {
      const bars = svg
        .selectAll("rect")
        .data(Object.values(self.analizedModules));

      bars
        .enter()
        .append("rect")
        .merge(bars)
        .attr("width", (d, i) => {
          return barWidth;
        })
        .attr("height", (d, i) => {
          return yScale(d) + "px";
        })
        .attr("x", (d, i) => {
          return i * (barWidth + padding);
        })
        .attr("y", (d, i) => {
          return height - yScale(d) - 17;
        })
        .attr("fill", "white")
        .attr("fill-opacity", d => {
          return opacityScale(d);
        })
        // tooltip
        .append("title")
        .text((d, i) => {
          const isPlural =
            Object.values(self.analizedModules)[i] > 1 ? "times" : "time";
          return `${Object.keys(self.analizedModules)[
            i
          ].toUpperCase()} module ${
            Object.values(self.analizedModules)[i]
          } ${isPlural} used`;
        });

      bars.exit().remove();
    })();

    // bar text (quantity label)
    (function barLabels() {
      const labels = svg
        .selectAll(".top-labels")
        .data(Object.values(self.analizedModules));

      labels
        .enter()
        .append("text")
        .merge(labels)
        .text((d, i) => {
          return d;
        })
        .attr("x", (d, i) => {
          return i * (barWidth + padding) + barWidth / 2;
        })
        .attr("y", (d, i) => {
          return height - yScale(d) - 20;
        })
        .attr("class", "top-labels")
        .style("text-anchor", "middle")
        .style("font-style", "italic");

      labels.exit().remove();
    })();

    // bottom labels (module names)
    (function bottomLabels() {
      const labels = svg
        .selectAll(".bottom-labels")
        .data(Object.values(self.analizedModules));

      labels
        .enter()
        .append("text")
        .merge(labels)
        .text((d, i) => {
          return Object.keys(self.analizedModules)[i];
        })
        .attr("x", (d, i) => {
          return i * (barWidth + padding) + barWidth / 2;
        })
        .attr("y", (d, i) => {
          return height - 4;
        })
        .attr("class", "bottom-labels")
        .style("font-size", "0.73rem")
        .attr("fill", "white")
        .style("text-anchor", "middle");

      labels.exit().remove();
    })();

    // Left axis
    (function leftAxis() {
      const upScale = d3
        .scaleLinear()
        .domain([0, d3.max(Object.values(self.analizedModules)) + 5])
        .range([height - 20, 0]);

      const yAxis = d3.axisLeft(upScale);

      svg
        .append("g")
        .attr("class", "y-axis")
        .attr("transform", "translate( " + -9 + ",3)")
        .call(yAxis);
    })();

    // Left axis label
    (function mainLabel() {
      svg
        .append("text")
        .text("Quantity of module usage")
        .attr("x", 350)
        .attr("y", 0)
        .attr("class", "chart-title");
    })();
  }

  analizeMethods() {
    const self = this;
    const width = 600;
    const height = 600;
    const color = d3.scaleOrdinal(d3.schemeCategory10);

    const svg = d3
      .select("#methodSvg svg")
      .attr("width", width)
      .attr("height", height);

    // Opacity scale
    const opacityScale = d3
      .scaleLinear()
      .domain([0, d3.max(Object.values(self.analizedMethods))])
      .range([0.2, 1]);

    // Pie chart
    (function pieChart() {
      const pie = d3.pie();
      const outerRadius = width / 2;
      const innerRadius = width / 5;
      const arc = d3
        .arc()
        .innerRadius(innerRadius)
        .outerRadius(outerRadius)
        .padAngle(0.01)
        .cornerRadius(10);

      const arcs = svg
        .selectAll("g.arch")
        .data(pie(Object.values(self.analizedMethods)))
        .enter()
        .append("g")
        .attr("class", "arc")
        .attr("transform", "translate(" + outerRadius + "," + height / 2 + ")");

      arcs
        .append("path")
        .attr("fill", "white")
        .attr("fill-opacity", d => {
          return opacityScale(d.data);
        })
        .attr("d", arc);

      // Labels
      arcs
        .append("text")
        .attr("transform", (d, i) => {
          return "translate(" + arc.centroid(d) + ")";
        })
        .attr("text-anchor", "text-middle")
        .text(function(d, i) {
          if (Object.values(self.analizedMethods)[i] > 1) {
            return Object.keys(self.analizedMethods)[i];
          }
        })
        .style("font-size", "0.75rem");

      // Tooltip
      arcs.append("title").text((d, i) => {
        if (Object.values(self.analizedMethods)[i] <= 1) {
          return `D3 ${Object.keys(self.analizedMethods)[
            i
          ].toUpperCase()} method ${
            Object.values(self.analizedMethods)[i]
          } time used`;
        } else {
          return Object.values(self.analizedMethods)[i] + " times used";
        }
      });
    })();

    // Middle label
    (function middleLabel() {
      svg
        .append("text")
        .text("Methods used")
        .attr("x", width / 2)
        .attr("y", height / 2)
        .attr("text-anchor", "middle")
        .attr("class", "chart-title");
    })();
  }
}

export default Core;
