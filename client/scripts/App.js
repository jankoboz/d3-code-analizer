// Vendors
import * as d3 from "d3";
window.d3 = d3;

// App
import d3methods from "./modules/_method-list.js";
import Core from "./modules/_core";

new Core(d3methods);
