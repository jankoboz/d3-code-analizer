"use strict";

const path = require("path");
const webpack = require("webpack");

module.exports = {
  entry: "./client/scripts/App.js",
  output: {
    filename: "Scripts.js",
    path: process.cwd() + "/client/temp/"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: { presets: ["es2015"] }
      }
    ]
  }
};
