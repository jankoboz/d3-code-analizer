"use strict";

const gulp = require("gulp");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");

gulp.task("styles", function() {
  return gulp
    .src("client/styles/Main.scss")
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(gulp.dest("./client/temp/"));
});
