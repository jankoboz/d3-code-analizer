'use strict';

const gulp     = require('gulp');
const webpack  = require('webpack');
const webpack_config = require('../webpack.config.js');

gulp.task('scripts', function(cb) {
   webpack(webpack_config, function(err, stats) {
      if (err) {
         console.log(err.toString());
      } else {
         console.log(stats.toString());
         cb();
      }
   });
});