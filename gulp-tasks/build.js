"use strict";

const gulp = require("gulp");
const del = require("del");
const minify = require("gulp-minify");
const cleanCss = require("gulp-clean-css");
const rename = require("gulp-rename");

gulp.task("deleteDistFolder", function() {
  return del("./client/dist");
});

gulp.task("minifyCss", ["deleteDistFolder"], function() {
  return gulp
    .src("./client/temp/Main.css")
    .pipe(cleanCss({ level: { 1: { specialComments: 0 } } }))
    .pipe(rename("styles-min.css"))
    .pipe(gulp.dest("./client/dist/"));
});

gulp.task("minifyJs", ["deleteDistFolder"], function() {
  return gulp
    .src("./client/temp/Scripts.js")
    .pipe(minify())
    .pipe(rename("scripts-min.js"))
    .pipe(gulp.dest("./client/dist/"));
});

gulp.task("build", ["deleteDistFolder", "minifyCss", "minifyJs"]);
