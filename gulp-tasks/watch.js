"use strict";

const gulp = require("gulp");
const watch = require("gulp-watch");
const nodemon = require("gulp-nodemon");
const browserSync = require("browser-sync").create();
const _nodemon = require("nodemon");

gulp.task("nodemon", cb => {
  var started = false;
  return nodemon({ script: "./api/server.js" }).on("start", () => {
    if (!started) {
      cb();
      started = true;
    }
  });
});

gulp.task("watch", ["nodemon"], () => {
  browserSync.init(null, {
    proxy: "http://127.0.0.1:5000",
    files: ["client/**/*.*"],
    browser: "google chrome",
    port: 3000
  });
  watch("./views/**/*.html").on("change", () => {
    _nodemon.emit("restart");
    browserSync.reload();
  });
  watch("./client/styles/**/*.scss", () => {
    gulp.start("styles");
  });
  watch("./client/scripts/**/*.js", () => {
    gulp.start("scripts");
  });
});
