# d3-module-finder

Analize d3 modules & methods used in your project.

Built with:

- Node.js

## Installation

- [with GitHub](https://github.com/jankoboz/d3-project-analizer.git)

### Requirements

- Node.js >= 8

## Contributing

If you can, please contribute by reporting issues, discussing ideas, or submitting pull requests with patches and new features. I do my best to respond to all issues and pull requests within a day or two.
